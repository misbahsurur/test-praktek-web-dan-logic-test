/*
 Navicat Premium Data Transfer

 Source Server         : KoneksiMisbah
 Source Server Type    : MySQL
 Source Server Version : 100411
 Source Host           : localhost:3306
 Source Schema         : testpraktekweb

 Target Server Type    : MySQL
 Target Server Version : 100411
 File Encoding         : 65001

 Date: 18/07/2022 15:34:44
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for failed_jobs
-- ----------------------------
DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE `failed_jobs`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp(0) NOT NULL DEFAULT current_timestamp(0),
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `failed_jobs_uuid_unique`(`uuid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of failed_jobs
-- ----------------------------

-- ----------------------------
-- Table structure for inventory
-- ----------------------------
DROP TABLE IF EXISTS `inventory`;
CREATE TABLE `inventory`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `harga` int(11) NOT NULL,
  `stok` int(11) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of inventory
-- ----------------------------
INSERT INTO `inventory` VALUES (1, 'Buku', 10000, 10, '2022-07-18 11:27:38', '2022-07-18 08:32:23');
INSERT INTO `inventory` VALUES (2, 'Pensil', 3000, 5, '2022-07-18 11:28:08', '2022-07-18 11:28:11');
INSERT INTO `inventory` VALUES (3, 'Penghapus', 2000, 5, '2022-07-18 11:28:30', '2022-07-18 11:28:33');
INSERT INTO `inventory` VALUES (4, 'Penggaris', 5000, 5, '2022-07-18 11:28:51', '2022-07-18 11:28:53');
INSERT INTO `inventory` VALUES (5, 'Bolpen', 7000, 5, '2022-07-18 11:29:17', '2022-07-18 11:29:20');

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES (1, '2014_10_12_000000_create_users_table', 1);
INSERT INTO `migrations` VALUES (2, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO `migrations` VALUES (3, '2019_08_19_000000_create_failed_jobs_table', 1);
INSERT INTO `migrations` VALUES (4, '2019_12_14_000001_create_personal_access_tokens_table', 1);
INSERT INTO `migrations` VALUES (5, '2022_07_18_040303_create_inventories_table', 1);
INSERT INTO `migrations` VALUES (6, '2022_07_18_065459_transaksi', 2);

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets`  (
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  INDEX `password_resets_email_index`(`email`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of password_resets
-- ----------------------------

-- ----------------------------
-- Table structure for personal_access_tokens
-- ----------------------------
DROP TABLE IF EXISTS `personal_access_tokens`;
CREATE TABLE `personal_access_tokens`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `last_used_at` timestamp(0) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `personal_access_tokens_token_unique`(`token`) USING BTREE,
  INDEX `personal_access_tokens_tokenable_type_tokenable_id_index`(`tokenable_type`, `tokenable_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of personal_access_tokens
-- ----------------------------

-- ----------------------------
-- Table structure for transaksi
-- ----------------------------
DROP TABLE IF EXISTS `transaksi`;
CREATE TABLE `transaksi`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `harga` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of transaksi
-- ----------------------------
INSERT INTO `transaksi` VALUES (1, 'Buku', 10000, 2, 20000, '2022-07-18 07:16:56', '2022-07-18 07:16:56');
INSERT INTO `transaksi` VALUES (2, 'Buku', 10000, 2, 20000, '2022-07-18 07:21:55', '2022-07-18 07:21:55');
INSERT INTO `transaksi` VALUES (3, 'Buku', 10000, 13, 130000, '2022-07-18 07:45:50', '2022-07-18 07:45:50');
INSERT INTO `transaksi` VALUES (4, 'Buku', 10000, 1, 10000, '2022-07-18 07:47:12', '2022-07-18 07:47:12');
INSERT INTO `transaksi` VALUES (5, 'Buku', 10000, 10, 100000, '2022-07-18 08:29:09', '2022-07-18 08:29:09');
INSERT INTO `transaksi` VALUES (6, 'Buku', 10000, 10, 100000, '2022-07-18 08:30:03', '2022-07-18 08:30:03');
INSERT INTO `transaksi` VALUES (7, 'Buku', 10000, 10, 100000, '2022-07-18 08:30:27', '2022-07-18 08:30:27');
INSERT INTO `transaksi` VALUES (8, 'Buku', 10000, 20, 200000, '2022-07-18 08:32:06', '2022-07-18 08:32:06');
INSERT INTO `transaksi` VALUES (9, 'Buku', 10000, 1, 10000, '2022-07-18 08:32:23', '2022-07-18 08:32:23');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp(0) NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `users_email_unique`(`email`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'Layla Stehr', 'gwalker@example.org', '2022-07-18 04:25:18', '$2y$10$yKhQMlefRTysaVgyiO2DzOqwnNF3QUZc2TPZwK4LhCceR3glrkPSm', 'a7TTxjBdIQ', '2022-07-18 04:25:18', '2022-07-18 04:25:18');
INSERT INTO `users` VALUES (2, 'Beaulah Crist Sr.', 'stracke.jed@example.com', '2022-07-18 04:25:18', '$2y$10$zIv1B8xaxsOdsOD8NOPX0eFpbwwePwFz.F41EAhQ0Qw32evbPQeEi', 'PbWVHMTJoQ', '2022-07-18 04:25:18', '2022-07-18 04:25:18');
INSERT INTO `users` VALUES (3, 'Earl Lakin', 'alowe@example.net', '2022-07-18 04:25:18', '$2y$10$yiCzFjBF5z/Zz2V/ueIXRe/Sh1wlo6kwr233HttmpPeqNCUcYjXMy', 'iy7zJRp9hH', '2022-07-18 04:25:18', '2022-07-18 04:25:18');
INSERT INTO `users` VALUES (4, 'Maegan Watsica II', 'bconroy@example.org', '2022-07-18 04:25:18', '$2y$10$j.VyH3w.GmGM8A//TDzhqOGCiBlwLn5LJ7prXna.a6mgs/r2i5iYG', 'bexpQ71aoC', '2022-07-18 04:25:18', '2022-07-18 04:25:18');
INSERT INTO `users` VALUES (5, 'Milton Hessel', 'jdicki@example.org', '2022-07-18 04:25:18', '$2y$10$KPfxh70zyhnqkR8Atxv1sefXNT67ki2e1UbhoJ1okKlbwu2NIHDBq', 'at0MId3yxH', '2022-07-18 04:25:18', '2022-07-18 04:25:18');

SET FOREIGN_KEY_CHECKS = 1;
