<div class="p2">
    <form action="{{ route('inventory.store') }}" method="post">
        @csrf
        <div class="form-group">
            <label>Nama Barang</label>
            <select class="form-select" aria-label="Default select example" name="nama" id="nama">
                <option selected>Pilih Barang</option>
                @foreach ($data as $item)
                <option value="{{ $item->id }}">{{ $item->nama }}</option>
                @endforeach
            </select>
            <label>Jumlah Item</label>
            <input type="number" name="stok" id="stok" class="form-control" placeholder="Masukkan Jumlah Item">
        </div>
        <div class="form-group mt-2">
            <button class="btn btn-primary" type="submit">Submit</button>
        </div>
    </form>
</div>
