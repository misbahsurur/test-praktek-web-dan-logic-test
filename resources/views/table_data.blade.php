<div class="row">
	<div class="col-lg-12">
		<table id="table_id" class="display">
		   <thead>
			  <tr>
                <th>No</th>
                <th>Nama Barang</th>
                <th>Harga</th>
                <th>Stok</th>
                <th>Aksi</th>
			  </tr>
		   </thead>
		   <tbody>
                @php
                    $no = 0;
                @endphp
                @foreach ($data as $item)
                @php
                    $no++;
                @endphp
				  <tr>
                    <td>{{ $no }}</td>
                    <td>{{ $item->nama }}</td>
                    <td>{{ $item->harga }}</td>
                    <td>{{ $item->stok }}</td>
                    <td>
                        <button class="btn btn-danger" onClick="destroy({{ $item->id }})">Delete</button>
                    </td>
				  </tr>
				  @endforeach
		   </tbody>
		</table>
	</div>
</div>
