<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Test Praktek Web</title>

    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.css">
</head>

<body>
    <div class="container mt-5">
        <div class="row">
            <div class="col-lg-12">
                <h1>Halaman Invetory</h1>
                <button class="btn btn-info" onClick="create()">+Insert Inventory</button>
                <button class="btn btn-success" onClick="beli()">+ Pembelian</button>
                <button class="btn btn-warning" onClick="jual()">+ Penjualan</button>
                <div id="table_data">
                    @include('table_data',['data' => $data])
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div id="page" class="p-2"></div>
                </div>
            </div>
        </div>
    </div>


    <!-- JavaScript Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"
        integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js"
        integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous">
    </script>
    <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.js"></script>
    <script>
        $(document).ready( function () {
            $('#table_id').DataTable();
        } );
    </script>
    <script>
        function beli() {
            $.get("{{ url('beli') }}", {}, function(data, status) {
                $("#exampleModalLabel").html('Transaksi Pembelian')
                $("#page").html(data);
                $("#exampleModal").modal('show');

            });
        }
        function jual() {
            $.get("{{ url('jual') }}", {}, function(data, status) {
                $("#exampleModalLabel").html('Transaksi Penjualan')
                $("#page").html(data);
                $("#exampleModal").modal('show');
            });
        }
        function create() {
            $.get("{{ route('inventory.create') }}", {}, function(data, status) {
                $("#exampleModalLabel").html('Insert Inventory')
                $("#page").html(data);
                $("#exampleModal").modal('show');
            });
        }
        function store() {
            $.ajax({
                type: "get",
                url: "{{ url('tambah') }}",
                data : {
                    nama : $('[name=nama]').val(),
                    harga : $('[name=harga]').val(),
                    stok : $('[name=stok]').val(),
                },
                success: function(data) {
                    $(".btn-close").click();
                    location.reload();
                }
            });
        }
        function destroy(id) {
            $.ajax({
                type: "delete",
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                url: "{{ url('inventory') }}/"+id,
                data : {
                    nama : $('[name=nama]').val(),
                    harga : $('[name=harga]').val(),
                    stok : $('[name=stok]').val(),
                },
                success: function(data) {
                    $(".btn-close").click();
                    location.reload();
                }
            });
        }
    </script>
</body>

</html>
