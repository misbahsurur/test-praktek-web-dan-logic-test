<div class="p2">
    <div class="form-group">
        <label>Nama Barang</label>
        <input type="text" name="nama" id="nama" class="form-control" placeholder="Masukkan Nama Barang">
        <label>Harga</label>
        <input type="number" name="harga" id="harga" class="form-control" placeholder="Masukkan Harga">
        <label>Stok</label>
        <input type="number" name="stok" id="stok" class="form-control" value="5" placeholder="Masukkan stok">
    </div>
    <div class="form-group mt-2">
        <button class="btn btn-success" onClick="store()">Tambah</button>
    </div>
</div>
