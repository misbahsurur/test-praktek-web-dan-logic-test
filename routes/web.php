<?php

use App\Http\Controllers\InventoryController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::resource('/inventory', 'App\Http\Controllers\InventoryController');
Route::get('/tambah',[InventoryController::class, 'tambah']);
Route::get('/beli', [InventoryController::class, 'beli']);
Route::get('/jual', [InventoryController::class, 'jual']);
Route::post('/inventory/penjualan', [InventoryController::class, 'penjualan'])->name('inventory.penjualan');
Route::get('/inventory/showjual/{id}', [InventoryController::class, 'showjual'])->name('inventory.showjual');
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
