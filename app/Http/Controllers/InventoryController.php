<?php

namespace App\Http\Controllers;

use App\Models\Inventory;
use App\Http\Requests\StoreInventoryRequest;
use App\Http\Requests\UpdateInventoryRequest;
use App\Models\Transaksi;
use Illuminate\Http\Request;

class InventoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Inventory::all();
        return view('index',compact('data'));
    }

    public function beli()
    {
        $data = Inventory::all();
        return view('beli',compact('data'));
    }

    public function jual()
    {
        $data = Inventory::all();
        return view('jual',compact('data'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('create');
    }

    public function tambah(Request $request)
    {
        Inventory::create($request->all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreInventoryRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = Inventory::find($request->nama);
        $total = $request->stok + $id->stok;
        Inventory::where('id',$id->id)->update([
            'stok' => $total,
        ]);
        $beli = new Transaksi;
        $beli->nama = $id->nama;
        $beli->harga = $id->harga;
        $beli->jumlah = $request->stok;
        $beli->total = $beli->harga * $beli->jumlah;
        $beli->save();
        $beli->id;
        return redirect()->route('inventory.show',$beli);
    }

    public function penjualan(Request $request)
    {
        $id = Inventory::find($request->nama);
        $total = $id->stok - $request->stok;
        Inventory::where('id',$id->id)->update([
            'stok' => $total,
        ]);
        $jual = new Transaksi;
        $jual->nama = $id->nama;
        $jual->harga = $id->harga;
        $jual->jumlah = $request->stok;
        $jual->total = $jual->harga * $jual->jumlah;
        $jual->save();
        $jual->id;
        return redirect()->route('inventory.showjual',$jual);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Inventory  $inventory
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Transaksi::find($id);
        return view('show')->with([
            'data' => $data,
        ]);
    }

    public function showjual($id)
    {
        $data = Transaksi::find($id);
        return view('showjual')->with([
            'data' => $data,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Inventory  $inventory
     * @return \Illuminate\Http\Response
     */
    public function edit(Inventory $inventory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateInventoryRequest  $request
     * @param  \App\Models\Inventory  $inventory
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateInventoryRequest $request, Inventory $inventory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Inventory  $inventory
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Inventory::findOrFail($id);
        $data->delete();

    	return 'sukses';
    }
}
